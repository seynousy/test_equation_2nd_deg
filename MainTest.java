/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import javafx.scene.control.TextField;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.loadui.testfx.GuiTest;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;
import org.loadui.testfx.GuiTest;

// Testrail imports
import com.gurock.testrail.APIClient;
import com.gurock.testrail.APIException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;
import java.util.HashMap;
import org.json.simple.JSONObject;

/**
 *
 * @author seynou
 */
public class MainTest extends ApplicationTest {
    private APIClient client = new APIClient("http://localhost/testrail/testrail");
    
    public MainTest() throws IOException {
        
	client.setUser("seynousy@gmail.com");
	client.setPassword("passer");
        
        JSONObject c = null;
        try {
            c = (JSONObject) client.sendGet("get_case/15");
        } catch (MalformedURLException ex) {
            Logger.getLogger(MainTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (APIException ex) {
            Logger.getLogger(MainTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Sync with testrail: "+ c.get("title"));
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        try {
            FxToolkit.hideStage();
            release(new KeyCode[]{});
            release(new MouseButton[]{});
        } catch (TimeoutException ex) {
            Logger.getLogger(MainTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/application/MainScene.fxml"));
        loader.setController(new Controller());
        Parent mainNode = loader.load();
        stage.setScene(new Scene(mainNode));
        stage.show();
        stage.toFront();
    }

    /**
     * @Test quand delta > 0 --- Test 15
     */
    @Test
    public void TestResultWhenDetlaPositive() {
        TextField firstOp = (TextField) GuiTest.find("#firstOp");
        clickOn("#firstOp");
        write("1");

        TextField secondOp = (TextField) GuiTest.find("#secondOp");
        clickOn("#secondOp");
        write("5");

        TextField thirdOp = (TextField) GuiTest.find("#thirdOp");
        clickOn("#thirdOp");
        write("4");
        clickOn((Button)GuiTest.find("#submitButton"));
        TextArea resultField = (TextArea) GuiTest.find("#resultField");
        
        if (resultField.getText().equals("Les solutions sont x1: -4.0 x2: -1.0 S = {-4.0 , -1.0}")) {
            Map data = new HashMap();
            data.put("status_id", 1);
            data.put("comment", "Test réussi quand delta > 0");
            try {
                JSONObject r = (JSONObject) client.sendPost("add_result_for_case/9/15", data);
            } catch (IOException | APIException ex) {
                Logger.getLogger(MainTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Map data = new HashMap();
            data.put("status_id", 5);
            data.put("comment", "Test échoué quand delta > 0");
            try {
                JSONObject r = (JSONObject) client.sendPost("add_result_for_case/9/15", data);
            } catch (IOException | APIException ex) {
                Logger.getLogger(MainTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }   
        assertEquals("Test when delta > 0 passed", "Les solutions sont x1: -4.0 x2: -1.0 S = {-4.0 , -1.0}", resultField.getText());   
    }
    
    /**
     * @Test quand delta < 0 --- Test 14
     */
    @Test
    public void TestResultWhenDetlaNegative() {
        TextField firstOp = (TextField) GuiTest.find("#firstOp");
        clickOn("#firstOp");
        write("5");

        TextField secondOp = (TextField) GuiTest.find("#secondOp");
        clickOn("#secondOp");
        write("2");

        TextField thirdOp = (TextField) GuiTest.find("#thirdOp");
        clickOn("#thirdOp");
        write("1");
        clickOn((Button)GuiTest.find("#submitButton"));
        TextArea resultField = (TextArea) GuiTest.find("#resultField");
        
        if (resultField.getText().equals("Le syst�me n'admet pas de solutions dans R")) {
            Map data = new HashMap();
            data.put("status_id", 1);
            data.put("comment", "Test réussi quand delta < 0");
            try {
                JSONObject r = (JSONObject) client.sendPost("add_result_for_case/9/14", data);
            } catch (IOException | APIException ex) {
                Logger.getLogger(MainTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Map data = new HashMap();
            data.put("status_id", 5);
            data.put("comment", "Test échoué quand delta < 0");
            try {
                JSONObject r = (JSONObject) client.sendPost("add_result_for_case/9/14", data);
            } catch (IOException | APIException ex) {
                Logger.getLogger(MainTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        assertEquals("Test when delta < 0 passed", "Le syst�me n'admet pas de solutions dans R", resultField.getText());   
    }
    
    /**
     * @Test avec des variables incorrects --- Test 16
     */
    @Test
    public void TestResultWithIncorrectValues() {
        TextField firstOp = (TextField) GuiTest.find("#firstOp");
        clickOn("#firstOp");
        write("a");

        TextField secondOp = (TextField) GuiTest.find("#secondOp");
        clickOn("#secondOp");
        write("b");

        TextField thirdOp = (TextField) GuiTest.find("#thirdOp");
        clickOn("#thirdOp");
        write("c");
        clickOn((Button)GuiTest.find("#submitButton"));
        TextArea resultField = (TextArea) GuiTest.find("#resultField");
        
        if (resultField.getText().equals("")) {
            Map data = new HashMap();
            data.put("status_id", 1);
            data.put("comment", "Test réussi quand delta < 0");
            try {
                JSONObject r = (JSONObject) client.sendPost("add_result_for_case/9/16", data);
            } catch (IOException | APIException ex) {
                Logger.getLogger(MainTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Map data = new HashMap();
            data.put("status_id", 5);
            data.put("comment", "Test échoué quand delta < 0");
            try {
                JSONObject r = (JSONObject) client.sendPost("add_result_for_case/9/16", data);
            } catch (IOException | APIException ex) {
                Logger.getLogger(MainTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        assertEquals("Test when delta < 0 passed", "", resultField.getText());   
    }
    
    /**
     * @Test quand delta < 0 --- Test 17
     */
    @Test
    public void cleanInputsAfterOperations() {
        TextField firstOp = (TextField) GuiTest.find("#firstOp");
        clickOn("#firstOp");
        write("1");

        TextField secondOp = (TextField) GuiTest.find("#secondOp");
        clickOn("#secondOp");
        write("1");

        TextField thirdOp = (TextField) GuiTest.find("#thirdOp");
        clickOn("#thirdOp");
        write("1");
        
        
        TextArea resultField = (TextArea) GuiTest.find("#resultField");
        resultField.setText("resultat");
        
        clickOn("#clearButton");
        
        if (firstOp.getText().equals(" ") && secondOp.getText().equals("0") && thirdOp.getText().equals("0") && resultField.getText().equals("")) {
            Map data = new HashMap();
            data.put("status_id", 1);
            data.put("comment", "Test réussi quand il s'agit d'effacer tout après calcul");
            try {
                JSONObject r = (JSONObject) client.sendPost("add_result_for_case/9/17", data);
            } catch (IOException | APIException ex) {
                Logger.getLogger(MainTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Map data = new HashMap();
            data.put("status_id", 1);
            data.put("comment", "Test échoué quand il s'agit d'effacer tout après calcul");
            try {
                JSONObject r = (JSONObject) client.sendPost("add_result_for_case/9/17", data);
            } catch (IOException | APIException ex) {
                Logger.getLogger(MainTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        assertTrue("Test quand il s'agit d'effacer tout après calcul: Passed", firstOp.getText().equals(" ") && secondOp.getText().equals("0") && thirdOp.getText().equals("0") && resultField.getText().equals(""));
    }
}
